package com.ap.employeetracking.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.ap.employeetracking.EmployeeListActivity;
import com.ap.employeetracking.domain.Employee;
import com.ap.employeetracking.service.EmployeeManager;
import com.ap.employeetracking.service.Impl.EmployeeManagerImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Async Task to fetch Employee List from file / database.
 * @author Aniket Pansare
 */
public class FetchEmployeeListTask extends AsyncTask<String,Void,Void> {

    private EmployeeListActivity activity;
    private Context mContext;
    private List<Employee> employeeList;

    public FetchEmployeeListTask(EmployeeListActivity a)
    {
        this.activity = a;
        this.mContext = a.getApplicationContext();
    }

    @Override
    protected Void doInBackground(String... strings) {

        EmployeeManager employeeManager = new EmployeeManagerImpl(mContext);
        employeeList = new ArrayList<Employee>();

        try {
            if(employeeManager.isFirstTimeFlag()) {
                employeeManager.readEmployeeResourceFile();
                employeeManager.setFirstTimeFlag(false);
            }

                employeeList.addAll(employeeManager.getEmployeeList());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        activity.fetchEmployeesCallback(employeeList);
    }
}
