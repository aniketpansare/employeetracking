package com.ap.employeetracking.security;

/**
 * Security manager Interface for future use.
 * @author Aniket Pansare
 */
public interface SecurityManager {
    boolean isEmail(String email) throws NullPointerException;

    boolean isAlphaSpace(String str) throws NullPointerException;

    boolean isPositiveInteger(String val) throws NullPointerException;

    boolean isFullName(String str) throws NullPointerException;

    boolean isAge(String val) throws NullPointerException;
}
