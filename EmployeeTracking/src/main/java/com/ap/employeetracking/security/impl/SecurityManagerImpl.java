package com.ap.employeetracking.security.impl;

import com.ap.employeetracking.security.SecurityManager;
import com.ap.employeetracking.util.GlobalConstants;

/**
 * Security Manager Class for Future use.
 * @author Aniket Pansare
 */

public class SecurityManagerImpl implements SecurityManager {

    /**
     * Email address - regex pattern
     * Reference -http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
     */
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Check if email is in valid format.
     * @param email
     * @return
     * @throws NullPointerException
     */
    @Override
    public boolean isEmail(String email) throws NullPointerException {
        if (email == null) {
            throw new  NullPointerException();
        }
        if(email.length() <= GlobalConstants.EMAIL_MAX_LENGTH && email.matches(EMAIL_PATTERN ))
        {
            return true;
        }
        return false;
    }

    /**
     * Check if the string contains only alphabets.
     * @param str
     * @return
     * @throws NullPointerException
     */
    @Override
    public boolean isAlphaSpace(String str) throws NullPointerException {
        if (str == null) {
            throw new  NullPointerException();
        }
        return str.matches("^[a-zA-Z ]+$");
    }

    /**
     * Check if value is an integer.
     * @param val
     * @return
     * @throws NullPointerException
     */
    @Override
    public boolean isPositiveInteger(String val) throws NullPointerException {
        if (val == null) {
            throw new  NullPointerException();
        }
        if(val.matches("^\\d+$") && Integer.parseInt(val) >= 0)
        {
            return true;
        }
        return false;
    }

    /**
     * Check if the string is within the Full Name Constraints.
     * @param str
     * @return
     * @throws NullPointerException
     */
    @Override
    public boolean isFullName(String str) throws NullPointerException {
        if (str == null) {
            throw new  NullPointerException();
        }
        return str.length()>0 && str.length()<=GlobalConstants.FULL_NAME_MAX_LENGTH && str.matches("^[a-zA-Z ]+$");
    }

    /**
     * Check if it is a valid human age.
     * @param val
     * @return
     * @throws NullPointerException
     */
    @Override
    public boolean isAge(String val) throws NullPointerException {
        if (val == null) {
            throw new  NullPointerException();
        }
        if(val.matches("^\\d+$") && Integer.parseInt(val) > 0 && Integer.parseInt(val) <= GlobalConstants.MAX_AGE)
        {
            return true;
        }
        return false;
    }
}
