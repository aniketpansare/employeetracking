package com.ap.employeetracking;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ap.employeetracking.domain.Employee;
import com.ap.employeetracking.security.impl.SecurityManagerImpl;
import com.ap.employeetracking.service.EmployeeManager;
import com.ap.employeetracking.service.Impl.EmployeeManagerImpl;

/**
 * Add Employee Activity.
 * @author Aniket Pansare
 */
public class AddEmployeeActivity extends Activity {

    /**
     * Application Context.
     */
    private Context mContext;

    /**
     * Add Employee Button.
     */
    private Button btnAddEmployee;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);

        mContext = getApplicationContext();//Get Application Context
        final EmployeeManager employeeManager = new EmployeeManagerImpl(mContext);
        final com.ap.employeetracking.security.SecurityManager sm= new SecurityManagerImpl();
        btnAddEmployee = (Button) findViewById(R.id.btnAddEmployee);

        //Add Listener to Add Employee Button.
        btnAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                String fullName = ((EditText) findViewById(R.id.txtFullName)).getText().toString();
                String email = ((EditText) findViewById(R.id.txtEmail)).getText().toString();
                String sAgeInYears = ((EditText) findViewById(R.id.txtAgeInYears)).getText().toString();

                if(sm.isFullName(fullName) && sm.isEmail(email) && sm.isAge(sAgeInYears))
                {
                        int ageInYears = Integer.parseInt(sAgeInYears);
                        Employee.Status status = Employee.Status.ACTIVE;
                        employeeManager.addEmployee(fullName,email,ageInYears,status);
                        finish();
                }
                else{
                    Toast.makeText(mContext, "Enter valid Name, Email and Age(1-150).", Toast.LENGTH_SHORT).show();
                }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Enter valid data.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
