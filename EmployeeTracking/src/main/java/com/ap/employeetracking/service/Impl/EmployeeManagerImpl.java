package com.ap.employeetracking.service.Impl;

import android.content.Context;
import android.content.SharedPreferences;
import com.ap.employeetracking.R;
import com.ap.employeetracking.domain.Employee;
import com.ap.employeetracking.service.EmployeeManager;
import com.ap.employeetracking.util.AppendableObjectOutputStream;
import com.ap.employeetracking.util.EmployeeComparator;
import com.ap.employeetracking.util.GlobalConstants;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Employee Manager Implementation Class.
 * Handles all Employee related actions.
 * @author Aniket Pansare
 */
public class EmployeeManagerImpl implements EmployeeManager {

    /******************************************************************
     *  Fields.
     *****************************************************************/
    private static final String TAG = "com.ap.employeetracking.EmployeeManagerImpl";
    private ObjectOutputStream out;  //To write Employee records to internal file.
    private ObjectInputStream in;    //To read Employee records from internal file.
    private static Context mContext;        //Application Context.

    /******************************************************************
     *  Constructors.
     *****************************************************************/

    /**
     * Constructor to create Employee Manager instance.
     * @param context
     */
    public EmployeeManagerImpl(Context context)
    {
        mContext=context;
    }

    /******************************************************************
     *  Custom Methods of Employee Manager Class.
     *****************************************************************/

    /**
     * Read records from the Employee records file in the 'res' folder.
     * The File is read only once. (when app is loaded for first time)
     * @throws IOException
     */
    @Override
    public void readEmployeeResourceFile() throws IOException {
        InputStream ins = mContext.getResources().openRawResource(R.raw.employeerecords);
        Scanner sc = new Scanner(ins);

        while(sc.hasNext())
        {
            Employee emp= Employee.stringToObject(sc.nextLine());
            addEmployee(emp.getFullName(),emp.getEmail(),emp.getAgeInYears(),emp.getStatus());
        }
        sc.close();
    }

    /**
     * Fetch All Employees from persistent store (in our case internal Employee records File).
     * Need to modify this method if SQLite database integration is planned.
     * @return List<Employee> - returns all Active Employees.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Override
    public List<Employee> getEmployeeList() throws IOException, ClassNotFoundException {
        initRead();
        List<Employee> employeeList = new ArrayList<Employee>();
        Set<Long> deletedEmployeeIds = getDeletedEmpIds();
        try {
            for (int i = 0; i < getEmployeeCount(); i++) {
                Employee emp = (Employee) in.readObject();
                if(!deletedEmployeeIds.contains(emp.getId())) //Check if Employee is deleted.
                    employeeList.add(emp);
            }
        }catch (EOFException e)
        {
            e.printStackTrace();
        }
        closeRead();
        Collections.sort(employeeList,new EmployeeComparator());
        return employeeList;
    }

    /**
     * Add Employee to persistent store. (Employee Records File)
     * Need to Modify this method once SQLite Database integration done.
     * @param fullName
     * @param email
     * @param ageInYears
     * @param status
     * @throws IOException
     */
    @Override
    public void addEmployee(String fullName, String email, int ageInYears, Employee.Status status)  throws IOException {

        initWrite();
        Employee emp = Employee.create(getEmployeeCount()+1, fullName,email, ageInYears,status);
        out.writeObject(emp);
        incrementEmployeeCount();  //Keep track of No of Employees.
        setDataSetChangedFlag(true);  //Set Flag as data set cjanged.
        closeWrite();
    }

    /**
     * Removes the employee from the employee records file and adds it to a Deleted File.
     * No need on SQLite database integration as we can just change the status field in employee object.
     * @param id
     * @throws IOException
     */
    @Override
    public void removeEmployee(long id) throws IOException {
        File file = mContext.getFileStreamPath(GlobalConstants.DELETED_EMPLOYEE_FILE);
        ObjectOutputStream delout;
        if(file.exists()) {
            FileOutputStream delfos = mContext.openFileOutput(GlobalConstants.DELETED_EMPLOYEE_FILE, Context.MODE_APPEND);
            delout = new AppendableObjectOutputStream(delfos);
        }
        else{
            FileOutputStream delfos = mContext.openFileOutput(GlobalConstants.DELETED_EMPLOYEE_FILE, Context.MODE_APPEND);
            delout = new ObjectOutputStream(delfos);
        }
        delout.writeLong(id);
        incrementDeletedEmployeeCount(); //Keep track of Number of Employees deleted.
        setDataSetChangedFlag(true); //Set Flag as data set changed.

        if (delout != null) {
            delout.flush();
            delout.close();
        }
    }

    /**
     * Get List of Employee Ids that are deleted.
     * @return Set<Long> returns set of employee ids.
     * @throws IOException
     */
    @Override
    public Set<Long> getDeletedEmpIds() throws IOException {
        File file = mContext.getFileStreamPath(GlobalConstants.DELETED_EMPLOYEE_FILE);
        Set<Long> deletedEmployeeIds = new HashSet<Long>();
        if(file.exists()) {
            FileInputStream fis = mContext.openFileInput(GlobalConstants.DELETED_EMPLOYEE_FILE);
            ObjectInputStream delin = new ObjectInputStream(fis);

            for (int i = 0; i < getDeletedEmployeeCount(); i++) {
                deletedEmployeeIds.add(delin.readLong());
            }
            if (in != null) {
                delin.close();
            }
        }
        return deletedEmployeeIds;
    }

    /***************************************************************************************************
     * Methods Accessing Shared Preferences.
     ***************************************************************************************************/

    /**
     * Increment Count when new employee added.
     */
    protected void incrementEmployeeCount() {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE).edit();
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        editor.putInt(GlobalConstants.COUNT, prefs.getInt(GlobalConstants.COUNT,0)+1);
        editor.commit();
    }

    /**
     * Fetch no of employees in employee records file.
     * @return
     */
    @Override
    public int getEmployeeCount() {
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        return prefs.getInt(GlobalConstants.COUNT, 0);
    }

    /**
     * Increment deleted employee count.
     */
    protected void incrementDeletedEmployeeCount() {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE).edit();
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        editor.putInt(GlobalConstants.DELETE_COUNT, prefs.getInt(GlobalConstants.DELETE_COUNT,0)+1);
        editor.commit();
    }

   /**
     * Get No of Employees Deleted.
     * @return
     */
    @Override
    public int getDeletedEmployeeCount() {
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        return prefs.getInt(GlobalConstants.DELETE_COUNT, 0);
    }

    /**
     * Set Flag to keep track if the Employee resource file is loaded.
     * Operation performed only when application loaded for firs time.
     * @param var
     */
    @Override
    public void setFirstTimeFlag(boolean var) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.EXECUTE_ONCE_FLAG, var);
        editor.commit();
    }

    /**
     * Check if the Employee resource file from res folder is loaded.
     */
    @Override
    public boolean isFirstTimeFlag() {
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        return prefs.getBoolean(GlobalConstants.EXECUTE_ONCE_FLAG, true);
    }


    /**
     * Set Flag to keep track of the data set changes, to prevent heavy database operations.
     * @param var
     */
    @Override
    public void setDataSetChangedFlag(boolean var) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE).edit();
        editor.putBoolean(GlobalConstants.DATA_SET_CHANGED, var);
        editor.commit();
    }

    /**
     * Check if the data set has changed.
     * @return
     */
    @Override
    public boolean isDataSetChangedFlag() {
        final SharedPreferences prefs = mContext.getSharedPreferences(GlobalConstants.SHARED_PREF_FILE, Context.MODE_PRIVATE);
        return prefs.getBoolean(GlobalConstants.DATA_SET_CHANGED, true);
    }

    /***************************************************************************************************
     * File operations.
     ***************************************************************************************************/

    /**
     * Opens the Employee records file in Append mode to add more employee records.
     * (No use once SQlite Integration Done.)
     * @throws IOException
     */
    private void initWrite() throws IOException {
        File file = mContext.getFileStreamPath(GlobalConstants.EMPLOYEE_RECORDS_FILE);
        if(file.exists()) {
            FileOutputStream fos = mContext.openFileOutput(GlobalConstants.EMPLOYEE_RECORDS_FILE, Context.MODE_APPEND);
            out = new AppendableObjectOutputStream(fos);
        }
        else{
            FileOutputStream fos = mContext.openFileOutput(GlobalConstants.EMPLOYEE_RECORDS_FILE, Context.MODE_APPEND);
            out = new ObjectOutputStream(fos);
        }
    }

    /**
     * Closes the Employee records file opened in write mode after flushing the write buffer.
     * @throws IOException
     */
    private void closeWrite() throws IOException {
        if (out != null) {
            out.flush();
            out.close();
        }
    }

    /**
     * Opens the Employee records file for reading.
     * @throws IOException
     */
    private void initRead() throws IOException {
        FileInputStream fis = mContext.openFileInput(GlobalConstants.EMPLOYEE_RECORDS_FILE);
        in = new ObjectInputStream(fis);
     }

    /**
     * Closes the Employee records file opened in read mode.
     * @throws IOException
     */
    private void closeRead() throws IOException {
        if(in!=null)in.close();
    }
}
