package com.ap.employeetracking.service;

import com.ap.employeetracking.domain.Employee;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Employee Manager Interface.
 * @author Aniket Pansare
 */
public interface EmployeeManager {

    /**
     * Read records from the Employee records file in the 'res' folder.
     * The File is read only once. (when app is loaded for first time)
     * @throws IOException
     */
    void readEmployeeResourceFile() throws IOException;

    /**
     * Fetch All Employees from persistent store (in our case internal Employee records File).
     * Need to modify this method if SQLite database integration is planned.
     * @return List<Employee> - returns all Active Employees.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    List<Employee> getEmployeeList() throws IOException, ClassNotFoundException;

    /**
     * Add Employee to persistent store. (Employee Records File)
     * Need to Modify this method once SQLite Database integration done.
     * @param fullName
     * @param email
     * @param ageInYears
     * @param status
     * @throws IOException
     */
    void addEmployee(String fullName, String email, int ageInYears, Employee.Status status) throws IOException;

    /**
     * Removes the employee from the employee records file and adds it to a Deleted File.
     * No need on SQLite database integration as we can just change the status field in employee object.
     * @param id
     * @throws IOException
     */
    void removeEmployee(long id) throws IOException;

    /**
     * Get List of Employee Ids that are deleted.
     * @return Set<Long> returns set of employee ids.
     * @throws IOException
     */
    Set<Long> getDeletedEmpIds() throws IOException;

    /**
     * Fetch no of employees in employee records file.
     * @return
     */
    int getEmployeeCount();

    /**
     * Get No of Employees Deleted.
     * @return
     */
    int getDeletedEmployeeCount();

    /**
     * Set Flag to keep track if the Employee resource file is loaded.
     * Operation performed only when application loaded for firs time.
     * @param var
     */
    void setFirstTimeFlag(boolean var);

    /**
     * Check if the Employee resource file from res folder is loaded.
     */
    boolean isFirstTimeFlag();

    /**
     * Set Flag to keep track of the data set changes, to prevent heavy database operations.
     * @param var
     */
    void setDataSetChangedFlag(boolean var);

    /**
     * Check if the data set has changed.
     * @return
     */
    boolean isDataSetChangedFlag();
}

