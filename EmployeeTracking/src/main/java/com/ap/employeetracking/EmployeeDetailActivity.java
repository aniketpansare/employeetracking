package com.ap.employeetracking;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ap.employeetracking.domain.Employee;
import com.ap.employeetracking.service.EmployeeManager;
import com.ap.employeetracking.service.Impl.EmployeeManagerImpl;

import java.io.IOException;

/**
 * Employee Details/Remove Employee Activity.
 * @author Aniket Pansare
 */
public class EmployeeDetailActivity extends Activity {

    /**
     * Application Context.
     */
    private Context mContext;

    /**
     * Add Employee Button.
     */
    private Button btnDeleteEmployee,btnBack;

    Employee employee;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_detail);

        mContext = getApplicationContext();
        final EmployeeManager employeeManager = new EmployeeManagerImpl(mContext);
        btnDeleteEmployee = (Button) findViewById(R.id.btnDeleteEmployee);
        btnBack = (Button) findViewById(R.id.btnBack);

        //Fetch Employee details from the intent when employee selected in the list.
        employee = Employee.stringToObject(getIntent().getExtras().getString("EmpDetails"));

        //Display Employee Details.
        displayEmployeeDetails(employee);

        //Add listener to delete/Remove employee button.
        btnDeleteEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    employeeManager.removeEmployee(employee.getId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finish();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }

    /**
     * Displays Employee details on the UI.
     * @param employee
     */
    private void displayEmployeeDetails(Employee employee)
    {
        TextView id = (TextView) findViewById(R.id.dId);
        TextView fullName = (TextView) findViewById(R.id.dFullName);
        TextView email = (TextView) findViewById(R.id.dEmail);
        TextView age = (TextView) findViewById(R.id.dAgeInYears);

        id.setText(id.getText()+ String.valueOf(employee.getId()));
        fullName.setText(fullName.getText()+ employee.getFullName());
        email.setText(email.getText()+ employee.getEmail());
        age.setText(age.getText()+String.valueOf(employee.getAgeInYears()));
    }
}
