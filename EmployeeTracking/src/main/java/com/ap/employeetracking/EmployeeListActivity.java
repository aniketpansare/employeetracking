package com.ap.employeetracking;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.ap.employeetracking.adapter.EmployeeArrayAdapter;
import com.ap.employeetracking.domain.Employee;
import com.ap.employeetracking.service.EmployeeManager;
import com.ap.employeetracking.service.Impl.EmployeeManagerImpl;
import com.ap.employeetracking.tasks.FetchEmployeeListTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Employee List Activity.
 * @author Aniket Pansare
 */
public class EmployeeListActivity extends ListActivity {

    /**
     * Application Context.
     */
    private Context mContext;

    /**
     * Employee List Adapter object;
     */
    EmployeeArrayAdapter listAdapter;

    /**
     * Add Employees Button.
     */
    private Button btnAddEmp;

    /**
     * Progress bar
     */
    private ProgressDialog progress;

    List<Employee> employeeList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        mContext = getApplicationContext();
        employeeList = new ArrayList<Employee>();

        //Initialize Employee List.
        //initializeEmployeeList();
        new FetchEmployeeListTask(this).execute();
        progress= new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setMessage("Fetching Employee List...");
        progress.show();


        //Set List Adapter
        listAdapter = new EmployeeArrayAdapter(mContext,employeeList);
        setListAdapter(listAdapter);

        btnAddEmp = (Button) findViewById(R.id.btnAddEmp);
        btnAddEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Start Next Activity if login successful.
                Intent i = new Intent(getApplicationContext(), AddEmployeeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
            }
        });

        //Set on click Listener on the List view.
        ListView listView = getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Start Next Activity if login successful.
                Intent i = new Intent(getApplicationContext(), EmployeeDetailActivity.class);
                i.putExtra("EmpDetails",employeeList.get(position).toString());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        EmployeeManager employeeManager = new EmployeeManagerImpl(mContext);

        new FetchEmployeeListTask(this).execute();
    }

    /**
     * Call back from Async Task - FetchEmployeeTask.
     * @param employees
     */
    public void fetchEmployeesCallback(List<Employee> employees)
    {
        progress.dismiss();
        employeeList = new ArrayList<Employee>();
        employeeList.addAll(employees);

        //Set List Adapter
        listAdapter = new EmployeeArrayAdapter(mContext,employeeList);
        setListAdapter(listAdapter);
    }


    /**
     * NOT IN USE (Just For Reference)- This task is performed as an async task.
     * Initialize Employee list from employee records file in the res folder.
     */
    private void initializeEmployeeList()
    {
        EmployeeManager employeeManager = new EmployeeManagerImpl(mContext);
        employeeList = new ArrayList<Employee>();
        if(employeeManager.isFirstTimeFlag()) {

            try {
                employeeManager.readEmployeeResourceFile();
                employeeList.addAll(employeeManager.getEmployeeList());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        employeeManager.setFirstTimeFlag(false);
    }
}
