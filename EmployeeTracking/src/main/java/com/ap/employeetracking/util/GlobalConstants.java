package com.ap.employeetracking.util;

/**
 * Class Containing all Global Constants.
 * @author Aniket Pansare
 */
public class GlobalConstants {
    /**
     * Name of the Employee records File storing Employee Objects.
     */
    public static final String EMPLOYEE_RECORDS_FILE = "EmployeeRecords1.txt";

    /**
     * Name of the file storing deleted employee ids.
     */
    public static final String DELETED_EMPLOYEE_FILE = "DeletedEmployeeIds.txt";

    /**
     * Name of the shared preferences file for the app.
     */
    public static final String SHARED_PREF_FILE = "SharedPreferences";

    /**
     * Shared Preferences variable to keep track of no of employees in file/database.
     */
    public static final String COUNT = "NoOfEmployees";

    /**
     * Shared Preferences variable to keep track of number of deleted employees.
     */
    public static final String DELETE_COUNT = "NoOfDeletedEmployees";

    /**
     * Shared Preferences variable to keep track, if the the employee records file from res folder is loaded.
     */
    public static final String EXECUTE_ONCE_FLAG = "ExecuteOnce";

    /**
     * Shared Preferences variable to keep track if the data set has changed.
     */
    public static final String DATA_SET_CHANGED = "DataSetChangedFlag";

    /**
     * Max Email Length.
     */
    public static final int EMAIL_MAX_LENGTH = 30;

    /**
     * Max Full Name Length.
     */
    public static final int FULL_NAME_MAX_LENGTH = 30;

    /**
     * Max Age.
     */
    public static final int MAX_AGE = 150;

}
