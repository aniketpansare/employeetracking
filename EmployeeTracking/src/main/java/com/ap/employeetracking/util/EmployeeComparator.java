package com.ap.employeetracking.util;

import com.ap.employeetracking.domain.Employee;

import java.util.Comparator;

/**
 * Comparator for sorting Employees on full name.
 * @author Aniket Pansare
 */
public class EmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee employee, Employee employee2) {
        //Ascending Sort
        return employee.getFullName().toLowerCase().compareTo(employee2.getFullName().toLowerCase());
    }
}
