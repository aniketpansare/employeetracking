package com.ap.employeetracking.util;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * To allow appending records to an Object Output Stream.
 * (Does not write header while appending records).
 * @author Aniket Pansare
 */
public class AppendableObjectOutputStream extends ObjectOutputStream {

    public AppendableObjectOutputStream(OutputStream output) throws IOException {
        super(output);
    }

    @Override
    public void writeStreamHeader() throws IOException {
        reset();
    }
}
