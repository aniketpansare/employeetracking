package com.ap.employeetracking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ap.employeetracking.R;
import com.ap.employeetracking.domain.Employee;

import java.util.List;

/**
 * Custom List Adapter for Employee List Activity.
 * @author AniketPansare
 */
public class EmployeeArrayAdapter extends ArrayAdapter<Employee> {

    /**
     * Store Application Context
     */
    private final Context context;

    /**
     * Reference to list of Employees in the list
     */
    private final List<Employee> employees;

    /**
     * Constructor.
     * @param context
     * @param employees
     */
    public EmployeeArrayAdapter(Context context, List<Employee> employees) {
        super(context, R.layout.employee_row_layout,employees);
        this.context = context;
        this.employees = employees;
    }

    /**
     * Get View Method to inflate each row in the list.
     * @param pos
     * @param convertView
     * @param viewGroup
     * @return
     */
    @Override
    public View getView(int pos, View convertView, ViewGroup viewGroup)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.employee_row_layout,viewGroup,false);

        //Fetch Views for the row
        TextView fullName = (TextView) rowView.findViewById(R.id.full_name);
        TextView email = (TextView) rowView.findViewById(R.id.email);
        TextView empId = (TextView) rowView.findViewById(R.id.employee_id);

        //Update Views
        fullName.setText(employees.get(pos).getFullName());
        email.setText(employees.get(pos).getEmail());
        empId.setText(String.valueOf(employees.get(pos).getId()));

        return rowView;
    }

}
