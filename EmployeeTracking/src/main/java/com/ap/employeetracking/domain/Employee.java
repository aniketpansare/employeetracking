package com.ap.employeetracking.domain;

import com.ap.employeetracking.domain.impl.EmployeeImpl;

import java.io.Serializable;

/**
 * Abstract Employee Domain Class.
 * @author Aniket Pansare
 */
public abstract class Employee implements Serializable {

    /***************************************************************
     * Enum Definitions for EmployeeImpl Class.
     **************************************************************/
    public enum Status{
        ACTIVE,
        DISABLED,
        DELETED  //For Future use to keep track of old employees.
    }

    //Delimiter for overriden toString Method.
    public static String delim = ",";

    /*******************************************************************
     *  Static Factory Methods to create Employee Objects.
     ******************************************************************/

    /**
     * Static Factory method acting as Full Constructor
     * @param id
     * @param fullName
     * @param email
     * @param ageInYears
     * @param status
     * @return
     */
    public static Employee create(long id,String fullName, String email, int ageInYears, Status status)
    {
        return new EmployeeImpl(id,fullName,email, ageInYears,status);
    }

    /**
     * Static Factory Method to create Employee object from a string representation.
     * @param str
     * @return
     * @throws NullPointerException
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public static Employee stringToObject(String str) throws NullPointerException, NumberFormatException,IllegalArgumentException
    {
        if(str==null) throw new NullPointerException();

        String[] substr = str.split(delim);

        if(substr.length<5) throw new IllegalArgumentException();

        long id = Long.parseLong(substr[0]);
        String fullName=substr[1];
        String email=substr[2];
        int ageInYears=Integer.parseInt(substr[3]);
        Status status=Status.valueOf(substr[4]);

        return new EmployeeImpl(id,fullName,email, ageInYears,status);
    }

    /******************************************************************
     *  Custom Overriden Methods.
     *****************************************************************/

    @Override
    public abstract String toString();

    /******************************************************************
     *  Interface Getter and Setter Methods.
     *****************************************************************/

    public abstract String getFullName();

    public abstract void setFullName(String fullName);

    public abstract long getId();

    public abstract void setId(long id);

    public abstract String getUsername();

    public abstract void setUsername(String username);

    public abstract String getEmail();

    public abstract void setEmail(String email);

    public abstract int getAgeInYears();

    public abstract void setAgeInYears(int ageInYears);

    public abstract Status getStatus();

    public abstract void setStatus(Status status);
}
