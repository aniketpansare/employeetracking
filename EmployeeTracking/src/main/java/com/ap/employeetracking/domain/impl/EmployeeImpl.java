package com.ap.employeetracking.domain.impl;

import com.ap.employeetracking.domain.Employee;

import java.io.Serializable;

/**
 * Employee Domain Class Implementation.
 * @author Aniket Pansare
 */
public class EmployeeImpl extends Employee implements Serializable{

    private static final long serialVersionUID = 1L;

    /**************************************************************************************
     * Employee Fields
     *************************************************************************************/
    private long id;  //Unique Identifier
    private String fullName;  //Full Name of the Employee.
    private String username;  //Username for Future Use
    private String email;     //Email id of the Employee
    private int ageInYears;   //Age of the Employee in years
    private Status status;    // Status of the employee for Future Use.

    /**************************************************************************************
     * Constructors
     *************************************************************************************/

    /**
     * Constructor
     * @param id
     * @param fullName
     * @param email
     * @param ageInYears
     * @param status
     */
    public EmployeeImpl(long id,String fullName, String email, int ageInYears, Status status) {
        this.id=id;
        this.fullName=fullName;
        this.email=email;
        this.ageInYears=ageInYears;
        this.status=status;
    }

    /***************************************************************************
     * Custom Methods
     ***************************************************************************/

    @Override
    public String toString()
    {
        return id+delim+fullName+delim+email+delim+ageInYears+delim+status;
    }


    /**************************************************************************************
     * Getter and Setter Methods
     *************************************************************************************/

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int getAgeInYears() {
        return ageInYears;
    }

    @Override
    public void setAgeInYears(int ageInYears) {
        this.ageInYears = ageInYears;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

}
